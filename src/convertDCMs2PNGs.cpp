#ifdef __APPLE__
#include <sys/uio.h>
#elif _WIN32
#include <io.h>
#else
#include <sys/io.h>
#endif

#include <cstdio>
#include <cstdlib>

// -- std ---
#include <string>
#include <vector>
#include <iostream>

using namespace std;

// -- other headers --
#include "rawprocessor.h"
#include "libdcm.h"
#include "png.h"

// -- file headers --
#include <dirent.h>

////////////////////////////////////////////////////////////////////////////////

#define MADE_DATE               "2017-05-19"

////////////////////////////////////////////////////////////////////////////////

const char sampledcm[] = "SIMFIT.dcm";
const char input_dir[] = "./input_images";

////////////////////////////////////////////////////////////////////////////////

int getDCMFiles( const char* fpath, vector<string> &file_names );
const char* extractFileExtension( const char* file_name );
bool save2monopng( const char* fpath, const char* buff, unsigned w, unsigned h );
void testDCM2RAW( const string* dfile );
void testDCM2PNG( const char* dcm_file );

////////////////////////////////////////////////////////////////////////////////


int main( int argc, char* argv[] )
{
    printf("----- %s, main Function is Started...! -----\n", MADE_DATE);
    
    extractFileExtension(sampledcm);
    
    vector<string> dcm_files;
    int cnt = getDCMFiles( input_dir, dcm_files );
    
    // testDCM2PNG("SIMFIT.dcm");
    
    if ( cnt > 2 )
    {
        for ( int i=2; i<cnt; i++ ) // 'i=2': '.' and '..' must be extracted.
        {
            testDCM2PNG( dcm_files[i].c_str() );
        }
    }
    else
    {
        printf("[Error: main func]  DCM File is not exist...!\n");
    }
    
    return 0;
}


int getDCMFiles( const char* fpath, vector<string> &file_names )
{
    if ( fpath == NULL ) {
        printf("[Error: getDCMFiles func]  Fail to get fpath...! \n");
        return -1;
    }
    
    DIR *dir;
    struct dirent *sdir;
    
    file_names.clear();
    
    dir = opendir(fpath);
    if ( dir != NULL ) {
        int i = 0;
        while ( ( sdir = readdir(dir) ) )
        {
            // printf("Filename: %s\n",sdir->d_name);
            string dcm_path = fpath;
            dcm_path += "/";
            dcm_path += sdir->d_name;
            file_names.push_back(dcm_path);
            // cout << file_names[i].c_str()  << endl;
            i++;
        }
    }
    else
    {
        printf("[Error: getDCMFiles func]  Input Directory does not exist...!\n");
    }
    
    // Free
    closedir(dir);
    
    // Return
    return file_names.size(); // dcm file count result is returned...
}

const char* extractFileExtension( const char* file_name )
{
    if ( file_name == NULL ) {
        printf ("[Error: extractFileExtension func]  Fail to get file_name...! \n");
    }
    
    // static string fname = file_name => 
    static string fname;
    fname = file_name;
    
    int fname_len = fname.size();
    int nExt = fname.rfind(".dcm");
    //printf("nExt: %d\n", nExt);
    fname.erase(nExt, fname_len);
    // cout << fname << endl;
    fname.insert(nExt, ".png");
    // cout << fname << endl;
    
    // Return
    return fname.c_str();
}


bool save2monopng( const char* fpath, const char* buff, unsigned w, unsigned h )
{
    if ( ( buff == NULL ) || ( w == 0 ) || ( h == 0 ) )
        return false;
    
    FILE* fp = fopen( fpath, "wb" );
    
    if ( fp == NULL )
        return false;
    
    png_structp png_ptr     = NULL;
    png_infop   info_ptr    = NULL;
    png_bytep   row         = NULL;
    
    png_ptr = png_create_write_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );
    if ( png_ptr != NULL )
    {
        info_ptr = png_create_info_struct( png_ptr );
        if ( info_ptr != NULL )
        {
            if ( setjmp( png_jmpbuf( (png_ptr) ) ) == 0 )
            {
                int mx = w;
                int my = h;
                
                png_init_io( png_ptr, fp );
                png_set_IHDR( png_ptr,
                             info_ptr,
                             mx,
                             my,
                             8,
                             PNG_COLOR_TYPE_GRAY,
                             PNG_INTERLACE_NONE,
                             PNG_COMPRESSION_TYPE_BASE,
                             PNG_FILTER_TYPE_BASE);
                
                png_write_info( png_ptr, info_ptr );
                
                row = (png_bytep)malloc( w * sizeof( png_byte ) );
                if ( row != NULL )
                {
                    int bque = 0;
                    
                    for( int y=0; y<my; y++ )
                    {
                        for( int x=0; x<mx; x++ )
                        {
                            //int bque = y * mx  + ( x * pd );
                            row[ x ] = buff[ bque ];
                            bque ++;
                        }
                        
                        png_write_row( png_ptr, row );
                    }
                    
                    png_write_end( png_ptr, NULL );
                    
                    fclose( fp );
                    
                    free(row);
                }
                
                png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
                png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
                
                return true;
            }
        }
    }
    
    return false;
}


void testDCM2PNG( const char* dcm_file )
{
    printf( "#TEST: DCM to RAW image# %s \n", dcm_file );
    
    if ( OpenDCM( dcm_file ) == true )
    {
        
        printf( "Ok.\n" );
        printf( "Check pixel data elements : " );
        
        ImageInformation iinfo = {0};
        
        if ( ReadPixelData( &iinfo ) == true )
        {
            printf( "Ok.\n" );
            
            // Convert DCM raw image info to RAW image processor.
            RAWProcessor* rawproc = new RAWProcessor();
            if ( rawproc != NULL )
            {
                int datasz = 2;
                if ( iinfo.bpp <= 8 )
                {
                    datasz = 1;
                }
                
                printf( "Converting to RAW image ... " );
                bool retb = \
                rawproc->LoadFromMemory( (const char*)iinfo.pixels,
                                        iinfo.width * iinfo.height * datasz,
                                        0, /// No transform
                                        iinfo.height );
                if ( retb == true )
                {
                    // Make RAW image to simply dumped as a file.
                    char tmpstr[128] = {0};
                    
                    sprintf( tmpstr, "rawdump_w%d_h%d.raw", iinfo.width, iinfo.height );
                    
                    if ( rawproc->SaveToFile( tmpstr ) == true )
                    {
                        printf( "Ok, dumped a file : %s\n", tmpstr );
                        printf( "raw image sizes : %d x %d\n", rawproc->Width(), rawproc->Height() );
                    }
                    else
                    {
                        printf( "Failed to dump a file.\n" );
                    }
                    
                    printf( "Finding window center and width ... " );
                    // And then, process a little.
                    // Find DICOM window center and width.
                    
                    DCMTagElement* dcmtag = NULL;
                    
                    unsigned w_center = 0;
                    unsigned w_width  = 0;
                    int      l_min    = 0;
                    int      l_max    = 0;
                    
                    dcmtag = FindElement( 0x00281050 ); /// window center
                    if ( dcmtag != NULL )
                    {
                        w_center = atoi( dcmtag->staticbuffer );
                    }
                    
                    dcmtag = FindElement( 0x00281051 ); /// window width
                    if ( dcmtag != NULL )
                    {
                        unsigned w_width = atoi( dcmtag->staticbuffer );
                        if ( ( w_width > 0 ) && ( w_center == 0 ) )
                        {
                            w_center = w_width / 2;
                        }
                        
                        l_max = w_center + w_width / 2;
                        l_min = w_center - w_width / 2;
                        
                        if ( l_min < 0 )
                        {
                            l_min = 0;
                        }
                    }
                    
                    if ( l_min < l_max )
                    {
                        printf( "Ok,\nProcessing to PNG image ... " );
                        
                        // Make thresholded image.
                        RAWProcessor::WeightAnalysisReport wreport = {0};
                        rawproc->GetAnalysisReport( wreport );
                        wreport.threshold_wide_min = l_min;
                        wreport.threshold_wide_max = l_max;
                        
                        vector< unsigned char > thresholded;
                        
                        if ( rawproc->Get8bitThresholdedImage( wreport, &thresholded ) == true )
                        {
                            const char* png_name = extractFileExtension( dcm_file );
                            if ( save2monopng( png_name,
                                              (const char*)thresholded.data(),
                                              rawproc->Width(),
                                              rawproc->Height() ) == true )
                            {
                                printf( "Ok.\n" );
                            }
                            else
                            {
                                printf( "Failure.\n" );
                            }
                            
                            thresholded.clear();
                        }
                        else
                        {
                            printf( "Failed to get thresholded image.\n" );
                        }
                    }
                    
                    delete rawproc;
                }
                else
                {
                    printf( "Failure.\n ");
                }
                
            }
        }
        else
        {
            printf( "Failure.\n" );
        }
        
        CloseDCM();
    }
}
