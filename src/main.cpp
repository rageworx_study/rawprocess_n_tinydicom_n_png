#ifdef __APPLE__
    #include <sys/uio.h>
#elif _WIN32
    #include <io.h>
#else
    #include <sys/io.h>
#endif

#include <cstdio>
#include <cstdlib>

// -- std ---
#include <string>
#include <vector>

using namespace std;

// -- other headers --
#include "rawprocessor.h"
#include "libdcm.h"
#include "png.h"

////////////////////////////////////////////////////////////////////////////////

#define MADE_DATE               "2017-04-18-1"

////////////////////////////////////////////////////////////////////////////////

const char sampledcm[] = "SIMFIT.dcm";

////////////////////////////////////////////////////////////////////////////////

bool save2monopng( const char* fpath, const char* buff, unsigned w, unsigned h )
{
    if ( ( buff == NULL ) || ( w == 0 ) || ( h == 0 ) )
        return false;

    FILE* fp = fopen( fpath, "wb" );

    if ( fp == NULL )
        return false;

    png_structp png_ptr     = NULL;
    png_infop   info_ptr    = NULL;
    png_bytep   row         = NULL;

    png_ptr = png_create_write_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );
    if ( png_ptr != NULL )
    {
        info_ptr = png_create_info_struct( png_ptr );
        if ( info_ptr != NULL )
        {
            if ( setjmp( png_jmpbuf( (png_ptr) ) ) == 0 )
            {
                int mx = w;
                int my = h;

                png_init_io( png_ptr, fp );
                png_set_IHDR( png_ptr,
                              info_ptr,
                              mx,
                              my,
                              8,
                              PNG_COLOR_TYPE_GRAY,
                              PNG_INTERLACE_NONE,
                              PNG_COMPRESSION_TYPE_BASE,
                              PNG_FILTER_TYPE_BASE);

                png_write_info( png_ptr, info_ptr );

                row = (png_bytep)malloc( w * sizeof( png_byte ) );
                if ( row != NULL )
                {
                    int bque = 0;

                    for( int y=0; y<my; y++ )
                    {
                        for( int x=0; x<mx; x++ )
                        {
                            //int bque = y * mx  + ( x * pd );
                            row[ x ] = buff[ bque ];
                            bque ++;
                        }

                        png_write_row( png_ptr, row );
                    }

                    png_write_end( png_ptr, NULL );

                    fclose( fp );

                    free(row);
                }

                png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
                png_destroy_write_struct(&png_ptr, (png_infopp)NULL);

                return true;
            }
        }
    }

    return false;
}

void testDCM()
{
    printf( "#TEST: Loading DCM #\n" );
    printf( "Loading DCM file : %s ... ", sampledcm );

    if ( OpenDCM( sampledcm ) == true )
    {
        printf( "Ok.\n" );

        printf( "Check elements : " );
        int elemsz = GetElementCount();
        printf( "%d elements found.\n", elemsz );

        if ( elemsz > 0 )
        {
            for( int cnt=0; cnt<elemsz; cnt++ )
            {
                DCMTagElement* elem = NULL;
                if ( GetElement( cnt, &elem ) > 0  )
                {
                    printf( "\t[%03d : %c%c] ID = %04X:%04X , size = %d bytes \n",
                            cnt + 1,
                            elem->VRtype[0],
                            elem->VRtype[1],
                            ( elem->id & 0xFFFF0000 ) >> 16,
                            elem->id & 0x0000FFFF,
                            elem->size );

                    delete elem;
                }
            }
        }

        CloseDCM();
    }
}

void testDCM2RAW()
{
    printf( "#TEST: DCM to RAW image#\n" );
    printf( "Loading DCM file : %s ... ", sampledcm );

    if ( OpenDCM( sampledcm ) == true )
    {
        printf( "Ok.\n" );

        printf( "Check pixel data elements : " );

        ImageInformation iinfo = {0};

        if ( ReadPixelData( &iinfo ) == true )
        {
            printf( "Ok.\n" );

            // Convert DCM raw image info to RAW image processor.

            RAWProcessor* rawproc = new RAWProcessor();

            if ( rawproc != NULL )
            {
                int datasz = 2;

                if ( iinfo.bpp <= 8 )
                {
                    datasz = 1;
                }

                printf( "Converting to RAW image ... " );
                bool retb = \
                rawproc->LoadFromMemory( (const char*)iinfo.pixels,
                                         iinfo.width * iinfo.height * datasz,
                                         0, /// No transform
                                         iinfo.height );
                if ( retb == true )
                {
                    // Make RAW image to simply dumped as a file.
                    char tmpstr[128] = {0};

                    sprintf( tmpstr, "rawdump_w%d_h%d.raw", iinfo.width, iinfo.height );

                    if ( rawproc->SaveToFile( tmpstr ) == true )
                    {
                        printf( "Ok, dumped a file : %s\n", tmpstr );

                        printf( "raw image sizes : %d x %d\n", rawproc->Width(), rawproc->Height() );
                    }
                    else
                    {
                        printf( "Failed to dump a file.\n" );
                    }

                    printf( "Finding window center and width ... " );
                    // And then, process a little.
                    // Find DICOM window center and width.

                    DCMTagElement* dcmtag = NULL;

                    unsigned w_center = 0;
                    unsigned w_width  = 0;
                    int      l_min    = 0;
                    int      l_max    = 0;

                    dcmtag = FindElement( 0x00281050 ); /// window center
                    if ( dcmtag != NULL )
                    {
                        w_center = atoi( dcmtag->staticbuffer );
                    }

                    dcmtag = FindElement( 0x00281051 ); /// window width
                    if ( dcmtag != NULL )
                    {
                        unsigned w_width = atoi( dcmtag->staticbuffer );
                        if ( ( w_width > 0 ) && ( w_center == 0 ) )
                        {
                            w_center = w_width / 2;
                        }

                        l_max = w_center + w_width / 2;
                        l_min = w_center - w_width / 2;

                        if ( l_min < 0 )
                        {
                            l_min = 0;
                        }
                    }

                    if ( l_min < l_max )
                    {
                        printf( "Ok,\nProcessing to PNG image ... " );

                        // Make thresholded image.
                        RAWProcessor::WeightAnalysisReport wreport = {0};
                        rawproc->GetAnalysisReport( wreport );
                        wreport.threshold_wide_min = l_min;
                        wreport.threshold_wide_max = l_max;

                        vector< unsigned char > thresholded;

                        if ( rawproc->Get8bitThresholdedImage( wreport, &thresholded ) == true )
                        {
                            if ( save2monopng( "converted_raw.png",
                                               (const char*)thresholded.data(),
                                               rawproc->Width(),
                                               rawproc->Height() ) == true )
                            {
                                printf( "Ok.\n" );
                            }
                            else
                            {
                                printf( "Failure.\n" );
                            }

                            thresholded.clear();
                        }
                        else
                        {
                            printf( "Failed to get thresholded image.\n" );
                        }
                    }

                    delete rawproc;
                }
                else
                {
                    printf( "Failure.\n ");
                }

            }
        }
        else
        {
            printf( "Failure.\n" );
        }

        CloseDCM();
    }
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int main( int argc, char** argv )
{
    printf( "librawprocessor + libtinydicom, examples : %s\n", MADE_DATE);
    printf( "(C)2017, Raph.K.\n");

    //testDCM();
    testDCM2RAW();

    return 0;
}
